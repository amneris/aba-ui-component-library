# ABA UI Component Library
All the shared UI components we use at ABA live in this repository. In the repo, each component has its own directory whose name matches the component’s npm package name. All components, and by extension ABA's npm package names, are prefixed with aba- for ABA English

## Tools used

https://github.com/JedWatson/generator-react-component