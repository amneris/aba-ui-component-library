import React from 'react';

export default class ProgressBar extends React.Component {

	render() {

		// Define the inline CSS styles for the progress bar here.
		var styles = {
			progressBarContainer: {
				background: "#f0f0f0", //todo: make global const
				borderRadius: 10,
				height: 10,
				position: "relative"
			},
			progressBarInner: {
				backgroundColor: "#07bbe4", //todo: make global const
				borderRadius: 10,
				height: 10,
				overflow: "hidden",
				position: "relative",
				width: this.props.percentage + '%'
			}
		}

		return (
			<div className="progressBar-container" style={styles.progressBarContainer}>
				<div className="progressBar-inner" style={styles.progressBarInner}/>
			</div>
		);
	}
}

ProgressBar.propTypes = {
	percentage: React.PropTypes.number.isRequired
}
