var React = require('react');
var ReactDOM = require('react-dom');
var ProgressBar = require('aba-react-progress-bar');

var App = React.createClass({
	render () {
		return (
			<div>
				<ProgressBar percentage={50} />
			</div>
		);
	}
});

ReactDOM.render(<App />, document.getElementById('app'));
